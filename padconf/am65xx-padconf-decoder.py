#!/usr/bin/python
#
# Copyright (c) 2019, Texas Instruments Incorporated
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# *  Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# *  Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# *  Neither the name of Texas Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

from __future__ import print_function
import xml.etree.ElementTree as ET
import re
import sys

# Inputs:
#   data - 32-bit register value
#   upper - Highest bit to keep
#   lower - Lowest bit to keep
#   (bit 0 refers to LSB, bit 31 to MSB)
# Return: right aligned data
def bits32(data, upper, lower):
    data = data >> lower;
    upper = upper - lower;
    bitmask =  0xFFFFFFFF >> (31 - upper);
    return (data & bitmask);

if len(sys.argv) != 2:
    print()  # Empty line
    print(sys.argv[0], "help:\n")
    print("Please pass the rd1 file to this script, e.g.:")
    print("", sys.argv[0], "am65xx-padconf_yyyy-mm-dd_hhmmss.rd1\n")
    print("Output file will have same base name as input, "
          "but csv file type.\n")
    sys.exit(1)

try:
    rd1 = open(sys.argv[1], "rt")
except IOError:
    print("Error: input file", sys.argv[1], "not accessible.")
    sys.exit(1)

try:
    csv_filename = sys.argv[1]
    csv_filename = csv_filename.replace(".rd1", ".csv")
    csv = open(csv_filename, "w+")
except IOError:
    print("Error creating file", csv_filename)
    sys.exit(1)

# CSV files must use \r\n for all line endings
# Create header row
csv.write("Ball,Reg Name,Reg Address,Reg Value,TX_DIS, SLEWRATE,"
          "RXACTIVE,Pull,Mux Mode\r\n")
csv_string = "%s,%s,0x%08X,0x%08X,%s,%s," \
             "%s,%s,%s\r\n"

# Determine device family based on header
header = rd1.readline()
m = re.match(r'.*AM(65)xx.*', header, 0)
if m:
    family = int(m.group(1))
else:
    print("Unrecognized device family in header:")
    print(header)
    sys.exit(1)

if family == 65:
    tree = ET.parse('AM654x_Pin_Mux.xml')
else:
    print("Didn't recognize device family AM%d" % family)
    print(header)
    sys.exit(1)

root = tree.getroot()

for lines in rd1:
    # Use regular expression to extract address and data from rd1
    m = re.match(r'(0x[0-9a-fA-F]{8})\s+(0x[0-9a-fA-F]{8})', lines, 0)
    if m:
        alphanum1 = m.group(1)  # address (string)
        alphanum2 = m.group(2)  # data (string)
        address = int(alphanum1, 16)  # convert from string to number
        register_value = int(alphanum2, 16)  # convert from string
        comments = ""  # Start new for each line

        # Derive Register Name
        if ((address & 0xFFFF0000) == 0x43010000):
            myregister_name = "CTRLMMR_WKUP_PADCONFIG"
            register_number = (address - 0x4301C000) >> 2
            myregister_name += "%d" % register_number
        elif ((address & 0xFFFF0000) == 0x00110000):
            myregister_name = "CTRLMMR_PADCONFIG"
            register_number = (address - 0x0011C000) >> 2
            myregister_name += "%d" % register_number
        else:
            print("Unexpected device address: 0x%08X" % address)
            sys.exit(1)

        # Use Register name to lookup ball number and muxmode in XML file
        muxmode = register_value & 0xF
        for clockNode_node in root.findall("./clockNode"):
            register_text = clockNode_node.find("./type/pad/confregisters/regbit/register").text
            if (register_text == myregister_name):
                ball = clockNode_node.find("./type/pad/ballname").text
                for muxmode_node in clockNode_node.findall("./type/pad/muxmode"):
                    muxmode_text = muxmode_node.find("./mode").text
                    if (muxmode_text != "Bootstrap"):
                        muxmode_int = int(muxmode_text)
                        if (muxmode_int == muxmode):
                            mux_mode_descr = muxmode_node.find("./signal").text

        # Decode TX_DIS field
        txdis = bits32(register_value,21,21)
        if (txdis == 0):
            txdis_string = "TX enabled"
        else:
            txdis_string = "TX disabled"

        # Decode SLEWRATE field
        slewrate = bits32(register_value,20,19)
        if (slewrate == 0):
            slewrate_string = "200 MHz"
        elif (slewrate == 1):
            slewrate_string = "150 MHz"
        elif (slewrate == 2):
            slewrate_string = "100 MHz"
        elif (slewrate == 3):
            slewrate_string = "50 MHz"

        # Decode RXACTIVE field
        rxactive = bits32(register_value,18,18)
        if (rxactive == 0):
            rxactive_string = "RX disabled"
        else:
            rxactive_string = "RX enabled"

        # Decode PULLTYPESEL and PULLUDEN fields
        pulltypesel = bits32(register_value,17,17)
        pulluden = bits32(register_value,16,16)
        if (pulluden == 0):
            pull_string = "pull disabled"
        elif (pulltypesel == 0):
            pull_string = "Internal Pulldown"
        elif (pulltypesel == 1):
            pull_string = "Internal Pullup"

        # Write a line of the CSV file
        csv.write(csv_string % (ball, myregister_name, address,
                  register_value, txdis_string, slewrate_string,
                  rxactive_string, pull_string, mux_mode_descr))

rd1.close()
csv.close()
