These Debug Server Scripting (DSS) files are intended to be invoked
from inside the Code Composer Studio (CCS) Scripting console.  These scripts have been tested
in CCS for both Windows and Linux.

Required Hardware
* Any "XDS" class debugger should be usable (XDS100v2, XDS110, XDS200, XDS560/XDS560v2)
* NOTE: 3rd party ARM debuggers lacking DAP support will not function (e.g. known issue with Segger)

Directions:

1. You first need to configure your debugger ccxml file.
   a. File -> New -> Target Configuration File
   b. Supply a name/location for the file.
   c. View -> Target Configurations to see the available target configurations (yours should now be among them!).
   d. Double-click your file in the Target Configurations panel to open it for editing.
   e. Select your emulator and processor. Be sure to select a processor and not a board,
      as we don't want any gel files to be part of the configuration. Save.
   f. Do NOT connect to any CPU cores.  The scripts handle this automatically once you get the debug session launched.

2. In the Target Configurations window, right-click on your ccxml file and select "Launch Selected Configuration".

3. Launch the scripting console by going to View -> Scripting Console.

4. Run the desired dss file using loadJSFile command, for example:

loadJSFile C:\Users\username\am65xx-dss-files\am65xx-ctt.dss

The resulting output file(s) will be on your Desktop.  Each generated file contains a timestamp in its name
such that you can invoke the scripts many times without overwriting previous files.

